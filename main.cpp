#include <stdio.h>
#include <SDL2/SDL.h>
#include <string>

#include "InputHandler.h"
#include "WindowHandler.h"
#include "globals.h"
#include "GameEvent.h"
#include "ScreenObject.h"
#include "Level.h"

//Creates a Level object for each line in gLEVELS_FILE andplace it it the gLevels vector.
//Those lines are paths of files with map data.
//See Level::loadMap for more information.
//Returns a pointer to the first Level object (currentLevel).
Level* loadLevels() {
	FILE* file = fopen(gLEVELS_FILE, "r");

	int f;
	std::string tempString;

	fscanf(file, "Number of files: %d", &f);

	for(int i = 1; i<=f; i++) {
		char tempCString[51];
		fscanf(file, "%50s", &tempCString);
		tempString = tempCString;
		Level level(tempString);
		gLevels.push_back(level);
	}

	fclose(file);
	file = nullptr;
	
	return &gLevels.front();
}

//Initializes the InputHandler and WindowHandler
//The WindowHandler intitilizes SDL
//Loads levels and returns a pointer to the first Level by calling loadLevels()
Level* init() {
	InputHandler::getInstance().init();
	if(!WindowHandler::getInstance().init()) {
		gRunning = false;
	}

	return loadLevels();
}

//While there are events in the eventQueue. Process those events. 
void update(float deltaTime, std::queue<GameEvent>& eventQueue) {
	GameEvent nextEvent;
	while(!eventQueue.empty()) {
		nextEvent = eventQueue.front();
		eventQueue.pop();

		if(nextEvent.action == ActionEnum::PLAYER_MOVE_UP){
			printf("w\n");
		}
	}
}

//This is the main draw function of the program. All calls to the WindowHandler for drawing purposes should originate here.
void draw(Level* currentLevel) {
	WindowHandler::getInstance().clear();
	WindowHandler::getInstance().drawList(currentLevel->getWalls());
	//Draw other things here...
	WindowHandler::getInstance().update();
}

//Calls cleanup code on program exit.
void close() {
	WindowHandler::getInstance().close();
}

int main(int argc, char *argv[]) {
	std::queue<GameEvent> eventQueue; //Main event queue for the program.
	Level* currentLevel = nullptr;
	float nextFrame = 1/gFpsGoal; //Time between frames in seconds
	float nextFrameTimer = 0.0f; //Time from last frame in seconds
	float deltaTime = 0.0f; //Time since last pass through of the game loop.
	auto clockStart = std::chrono::high_resolution_clock::now(); //Clock used for timing purposes
	auto clockStop = clockStart;

	currentLevel = init();

	while(gRunning) {
		clockStart = std::chrono::high_resolution_clock::now();

		InputHandler::getInstance().readInput(eventQueue);
		update(deltaTime, eventQueue);

		if(nextFrameTimer >= nextFrame) {
			draw(currentLevel);
			nextFrameTimer = 0.0f;
		}

		clockStop = std::chrono::high_resolution_clock::now();
		deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(clockStop - clockStart).count();
		nextFrame += deltaTime;
	}

	close();
	return 0;
}